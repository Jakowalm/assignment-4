# PokéTrainer

##Features

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.3.

Pokéball icon found at [https://www.freeiconspng.com/downloadimg/45330](https://www.freeiconspng.com/downloadimg/45330)

Infinite scrolling code based off of a response to [this](https://stackoverflow.com/questions/46913337/angular-2-scroll-down-call-function-load-more) stack overflow thread.

The project is based off of the [PokeAPI](https://pokeapi.co/) and the sprites can be found at [https://github.com/PokeAPI/sprites](https://github.com/PokeAPI/sprites).

Because of how many sprites are in the repo, the compiler will throw an error for too many files open.
To fix this, you can delete the`pokemon-sprites/sprites/items`folder as well as the sub-directories in the `pokemon-sprites/sprites/pokemon` folder.

##Use
The app is pretty simple to use. 
To log in, a username is required and anything will be accepted here.
Then, the user is taken to their collection page where they can see any pokemon they might have.
If they click the button to find pokémon, they will be taken to the pokédex where they can scroll through a list with all the available pokémon. 
Upon clicking a pokémon, it will be added to the user's collection.
On the collection page, a pokémon can be clicked to remove it from the list as well :)
