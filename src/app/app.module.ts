import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from "@angular/common/http";

import { AppComponent } from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {LoginModule} from "./login/login.module";
import {PokedexModule} from "./pokedex/pokedex.module";
import {AccessGuard} from "./shared/access-guard.service";
import {PokecollectionModule} from "./pokecollection/pokecollection.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    LoginModule,
    PokedexModule,
    PokecollectionModule
  ],
  providers: [AccessGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
