import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {PokecollectionRoutingModule} from "./pokecollection-routing.module";
import {CommonModule} from "@angular/common";
import {PokecollectionPage} from "./pages/pokecollection.page";

@NgModule({
  declarations: [
    PokecollectionPage
  ],
  imports: [
    PokecollectionRoutingModule,
    FormsModule,
    CommonModule
  ]
})
export class PokecollectionModule {

}
