import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PokecollectionPage} from "./pages/pokecollection.page";

// Inherit from AppRoutingModule -> /login
const routes: Routes = [
  {
    path: '',
    component: PokecollectionPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild( routes )
  ],
  exports: [
    RouterModule
  ]
})
export class PokecollectionRoutingModule {
}
