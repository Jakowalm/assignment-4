import { Component} from '@angular/core';
import {Router} from "@angular/router";
import {Pokemon} from "../../shared/pokemon.model";
import {User} from "../../shared/user.model";

@Component({
  selector: 'app-pokecollection',
  templateUrl: './pokecollection.page.html',
  styleUrls: ["./pokecollection.page.css","../../shared/shared.style.css"]
})
export class PokecollectionPage {

  public username: string;
  public usersPokemon: Pokemon[] = [];
  public allUsers: User[] = [];

  constructor(private readonly router: Router) {
    this.username = localStorage.getItem("loggedInUser")! //Will never be null because of the authentication.
    const str = localStorage.getItem("pokeTrainerUsers")
    if (str) {
      this.allUsers = JSON.parse(str);
      for (let user of this.allUsers) {
        if (user.username === this.username) {
          this.usersPokemon = user.collectedPokemon
          break;
        }
      }
    }
  }

  //If a pokémon in the collection is clicked, it is removed from the collection.
  public clickPokemon(pokemon: Pokemon): void {
    this.usersPokemon.splice(this.usersPokemon.indexOf(pokemon),1);
    pokemon.caught = false;
  }

  get pokemon(): Pokemon[] {
    return this.usersPokemon;
  }

  public findPokemon() : Promise<boolean> {
    return this.router.navigate(["pokedex"]);
  }

  public logOut(): void {
    localStorage.removeItem("loggedInUser");
    this.router.navigate(["login"]);
  }

}
