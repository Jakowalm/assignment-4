import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {PokeAPI} from "../api/pokedex.api";
import {Pokemon} from "../../shared/pokemon.model";
import {User} from "../../shared/user.model";

@Component({
  selector: 'app-pokedex-page',
  templateUrl: './pokedex.page.html',
  styleUrls: ["./pokedex.page.css","../../shared/shared.style.css"]
})
export class PokedexPage {

  public username: string;
  public userIndex: number = 0;
  public _pokemon: Pokemon[] = [];
  public usersPokemon: Pokemon[] = [];
  public allUsers: User[] = [];
  private readonly pokeApi: PokeAPI;
  private nextURL: string = "https://pokeapi.co/api/v2/pokemon?limit=20";
  private prevURL: string = ""
  public loadMore: Function;

  constructor(private readonly router: Router, private readonly pokeAPI: PokeAPI) {
    this.pokeApi = pokeAPI;
    this.loadMore = () => { //Method used in the InfiniteScrolling directive. pretty much just an api call.
      if (this.nextURL.length > 0 && this.nextURL !== this.prevURL) {
        this.prevURL = this.nextURL
        this.pokeAPI.getPokemon(this.nextURL)
          .subscribe((pokes) => {
            this.nextURL = pokes.next
            this.parsePokemon(pokes.results);
          })
      }
    }
    this.username = localStorage.getItem("loggedInUser")! //Will never be null because of the authentication.
    this.pokeAPI.getPokemon(this.nextURL)
      .subscribe((pokes) => { //API call to find the first 20 pokémon
        this.nextURL = pokes.next
        this.parsePokemon(pokes.results);
      })
    const str = localStorage.getItem("pokeTrainerUsers")
    if (str) { //Checks if the current user already exists and has pokémon.
      this.allUsers = JSON.parse(str);
      this.userIndex = this.allUsers.length
      for (let i = 0; i < this.allUsers.length; i++) {
        if (this.allUsers[i].username === this.username) {
          this.userIndex = i;
          this.usersPokemon = this.allUsers[i].collectedPokemon
          break;
        }
      }
    }
  }

  //Takes the results from the API call and parses it to pokémon before putting it into the list.
  public parsePokemon(result: any): void {
    for (let elem of result) {
      const name: string = elem.name[0].toUpperCase()+elem.name.slice(1)
      const temp = elem.url.split("/")
      const id = temp[temp.length-2]
      let caught = false;
      for (let p of this.usersPokemon) {
        if (p.name === name) {caught = true; break;}
      }
      this._pokemon.push({name: name, id: id, caught: caught})
    }
  }

  //Creates a string with all the user data.
  private usersToString() : string {
    this.allUsers[this.userIndex] = {
      username: this.username,
      collectedPokemon: this.usersPokemon
    }
    return JSON.stringify(this.allUsers);
  }

  //Stores a pokemon that's been clicked to the users and localStorage
  public clickPokemon(pokemon: Pokemon): void {
    pokemon.caught = true;
    this.usersPokemon.push(pokemon);
    localStorage.setItem("pokeTrainerUsers",this.usersToString());
  }

  get pokemon(): Pokemon[] {
    return this._pokemon;
  }

  public logOut(): void {
    localStorage.removeItem("loggedInUser");
    this.router.navigate(["login"]);
  }

  public toCollection(): void {
    this.router.navigate(["collection"]);
  }

}
