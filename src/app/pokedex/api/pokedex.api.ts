import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokeAPI {

  constructor(private readonly http: HttpClient) {
  }

  public getPokemon(getURL: string): Observable<any> {
    return this.http.get<any>(getURL);
  }

}
