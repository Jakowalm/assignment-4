import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {PokedexPage} from "./pages/pokedex.page";
import {PokedexRoutingModule} from "./pokedex-routing.module";
import {CommonModule} from "@angular/common";
import {InfiniteScrollDirective} from "./directives/infinite-scroll.directive";

@NgModule({
  declarations: [
    PokedexPage,
    InfiniteScrollDirective
  ],
  imports: [
    PokedexRoutingModule,
    FormsModule,
    CommonModule
  ]
})
export class PokedexModule {

}
