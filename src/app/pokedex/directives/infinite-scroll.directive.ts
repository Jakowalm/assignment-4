import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[infiniteScroll]',
})
export class InfiniteScrollDirective {

  private scrollThreshold = -1800; // px

  constructor(private element: ElementRef) {}
  private prevScroll: number = 0;
  private scrollTimes: number = 1;

  @Input('infiniteScroll') loadMore: any;

  @HostListener('scroll')
  public onScroll() {
    const scrolled = this.element.nativeElement.scrollTop;

    //Debounce that ensures that the API isn't spammed with requests.
    if (this.prevScroll - scrolled < this.scrollThreshold*(this.scrollTimes)) {
      this.scrollTimes+=0.03;
      this.prevScroll = scrolled;
      this.loadMore();
    }

  }

}
