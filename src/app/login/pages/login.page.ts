import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.page.html',
  styleUrls: ["./login.page.css","../../shared/shared.style.css"]
})
export class LoginPage {

  constructor(private readonly router: Router) {
  }

  //Redirects to the pokemon collection Page.
  public handleUserLoggedIn() : Promise<boolean> {
    return this.router.navigate(["collection"]);
  }

}
