import {Component, EventEmitter, Output} from "@angular/core";
import {Pokemon} from "../../shared/pokemon.model";

@Component({
  selector: "app-login-form",
  templateUrl: "login-form.component.html",
  styles: [
    `input {
      background-color: #D6D6D6;
      border-color: #525252;
      border-style: solid;
      border-width: 2px ;
      height: 40px;
      font-size: 150%;
      width: 280px;
      float: left;
      margin-left: 12px;
      font-family: 'Oswald', sans-serif;
    }`,
    `h2 {
      height: 20px;
      font-family: 'Oswald', sans-serif;
      margin-left: 12px;
      margin-top: 200px;
    }`,
    `button {
      font-family: 'Sigmar One', cursive;
      height: 46px;
      width: 46px;
      background-color: #85C7F2;
      border-color: #0B0014;
      float:left;
      margin-left: 5px;
    }`
  ]
})
export class LoginFormComponent{

  public username: string = "";

  @Output() loggedIn: EventEmitter<Pokemon> = new EventEmitter<Pokemon>();

  //Stores that the user has logged in and signals to the Login Page.
  public onUserSubmit() {
    localStorage.setItem("loggedInUser",this.username)
    this.loggedIn.emit();
  }

}
