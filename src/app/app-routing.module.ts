import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {AccessGuard} from "./shared/access-guard.service";

const routes: Routes = [ //The paths to collection and pokedex have the AccessGuard enabled and will run it before opening the module.
  { path: 'login',
    loadChildren: () => import("./login/login.module").then(m => m.LoginModule)
  },
  { path: 'pokedex', canActivate:[AccessGuard], data:{requiresLogin: true},
    loadChildren: () => import("./pokedex/pokedex.module").then(m => m.PokedexModule)
  },
  { path: 'collection', canActivate:[AccessGuard], data:{requiresLogin: true},
    loadChildren: () => import("./pokecollection/pokecollection.module").then(m => m.PokecollectionModule)
  },
  { path: '', redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule {}
