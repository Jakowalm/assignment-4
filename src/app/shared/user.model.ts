import {Pokemon} from "./pokemon.model";

export interface User {
  username: string,
  collectedPokemon: Pokemon[]
}
