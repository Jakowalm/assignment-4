import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router} from "@angular/router";
import {Observable} from "rxjs";

@Injectable()
export class AccessGuard implements CanActivate {
  constructor(private readonly router: Router) {
  }

  //If injected in a module, it ensures that the user has logged in.
  canActivate(route: ActivatedRouteSnapshot): Observable<boolean>|Promise<boolean>|boolean {
    const requiresLogin = route.data.requiresLogin || false;
    if (requiresLogin) {
      const login = localStorage.getItem("loggedInUser")
      if (login === null) return this.router.navigate(["login"])
      return true;
    }
    //If the user isn't logged in, they're redirected to the login page.
    return this.router.navigate(["login"]);
  }
}
